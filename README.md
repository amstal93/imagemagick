# [ImageMagick][project]

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

[ImageMagick][imagemagick] in Alpine.

## Usage

```bash
docker run -v "$(pwd):/images" --rm -d joshava/imagemagick mogrify -format png *.tif
```

[docker]: https://hub.docker.com/r/joshava/imagemagick
[docker_pull]: https://img.shields.io/docker/pulls/joshava/imagemagick.svg
[docker_star]: https://img.shields.io/docker/stars/joshava/imagemagick.svg
[docker_size]: https://img.shields.io/microbadger/image-size/joshava/imagemagick.svg
[docker_layer]: https://img.shields.io/microbadger/layers/joshava/imagemagick.svg
[license]: https://gitlab.com/joshua-avalon/docker/imagemagick/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/joshua-avalon/docker/imagemagick/pipelines
[gitlab_ci]: https://gitlab.com/joshua-avalon/docker/imagemagick/badges/master/pipeline.svg
[imagemagick]: https://www.imagemagick.org/
[project]: https://gitlab.com/joshua-avalon/docker/imagemagick
